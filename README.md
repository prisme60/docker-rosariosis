Docker RosarioSIS
=================

A Dockerfile that installs the latest (currently V4.1) [RosarioSIS](https://www.rosariosis.org/). This file pulls from a tag of the default branch.

![Image of Containers](http://www.plantuml.com/plantuml/proxy?src=https://gitlab.com/prisme60/docker-rosariosis/raw/master/doc/containers.pu)

## Installation

Minimum requirements: [Docker](https://www.docker.com/), [docker-compose](https://docs.docker.com/compose/) & Git working.

```bash
$ git clone https://gitlab.com/prisme60/docker-rosariosis.git
$ cd docker-rosariosis
$ docker-compose build
$ docker-compose up --build --no-start 
```

If you want to check operation state, type the following command:
```bash
$ docker-compose images
       Container               Repository         Tag       Image Id      Size 
-------------------------------------------------------------------------------
docker-rosariosis_db_1    sameersbn/postgresql   9.5      54cdeac08285   222 MB
docker-rosariosis_web_1   rosario_web            4.1_fr   a51ef8695d19   677 MB
```

## Preconfiguration

Edit the file `docker-compose.yml` in order to change :
- the email address of the administrator.
- the langage to use
- the hostname (by default 0.0.0.0 is set, it permits to be exposed on all network interfaces of the system)

## Usage

So docker-compose will start first PostgreSQL server (RosarioSIS uses a PostgreSQL database), because the web server depends on it, then the script of the web server check the database connectivity and initialize the database (TODO: check if the base is already initialized, the fact the rosariosis.sql is reloaded doesn't alter the current database)

```bash
$ docker-compose start
Starting db  ... done
Starting web ... done
$ docker-compose logs -f
```

Type Ctrl-C if you want to stop watching the log.
You can check that the containers are running with the following command :

```bash
$ docker ps
CONTAINER ID        IMAGE                      COMMAND                  CREATED             STATUS              PORTS                NAMES
d8da2ddd29cd        rosario_web:4.1_fr         "/init apache2-foreg…"   9 minutes ago       Up 29 seconds       0.0.0.0:80->80/tcp   docker-rosariosis_web_1
65a193b23df5        sameersbn/postgresql:9.5   "/sbin/entrypoint.sh"    9 minutes ago       Up 30 seconds       5432/tcp             docker-rosariosis_db_1
```

Port 80 will be exposed, so you can visit `localhost` to get started. The default username is `admin` and the default password is `admin`.

## Apply database language translation

If you want the French translation of the database content, you need to apply the rosariosis_fr.sql script on the database, when the webserver container is running:
1. Identify the webserver container (rosario_web)
```bash
$ docker ps
CONTAINER ID        IMAGE                      COMMAND                  CREATED             STATUS              PORTS                NAMES
d8da2ddd29cd        rosario_web:4.1_fr         "/init apache2-foreg…"   11 minutes ago      Up About a minute   0.0.0.0:80->80/tcp   docker-rosariosis_web_1
65a193b23df5        sameersbn/postgresql:9.5   "/sbin/entrypoint.sh"    11 minutes ago      Up About a minute   5432/tcp             docker-rosariosis_db_1
```

2. Execute the following command on the database container:
```bash
docker exec -i d8da2ddd29cd psql -f /usr/src/rosariosis/rosariosis_fr.sql
```

## Enviroment Variables

The RosarioSIS image uses several environment variables which are easy to miss. While none of the variables are required, they may significantly aid you in using the image.

### PGHOST

Host of the postgres data.

### PGUSER

This optional environment variable is used in conjunction with PGPASSWORD to set a user and its password for the default database that is used by RosarioSIS to store data.

### PGPASSWORD

This optional environment variable is used in conjunction with PGUSER to set a user and its password for the default database that is used by RosarioSIS to store data.

### PGDATABASE

This optional environment variable can be used to define a different name for the default database that is used by RosarioSIS to store data.

### PGPORT

This optional environment variable can be used to define a different port for the default database that is used by RosarioSIS to store data.

### ROSARIOSIS_YEAR

This optional environment variable can be used to define a year in the RosarioSIS settings.

### ROSARIOSIS_LANG

This optional environment variable is used for make RosarioSIS to show in another language.

### ROSARIOSIS_VERSION

This optional environment variable is used to set the required version of RosarioSIS.

### ROSARIOSIS_ADMIN_EMAIL:
This optional environment variable is used to set the email address of the RosarioSIS administrator.

## SMTP

RosarioSIS will attempt to send mail via the host's port 25. In order for this to work you must set the hostname of the rosariosis container to that of `host` (or some other hostname that your can appear on a legal `FROM` line) and configure the host to accept SMTP from the container. For postfix this means adding the container IP addresses to `/etc/postfix/main.cf` as in:

```
mynetworks = 192.168.0.0/16 172.16.0.0/12 10.0.0.0/8 127.0.0.0/8 [::ffff:127.0.0.0]/104 [::1]/128
```

## Save the RosarioSIS database

If you want to save the rosarioSIS database:
1. Identify the webserver container (rosario_web)
```bash
$ docker ps
CONTAINER ID        IMAGE                      COMMAND                  CREATED             STATUS              PORTS                NAMES
d8da2ddd29cd        rosario_web:4.1_fr         "/init apache2-foreg…"   11 minutes ago      Up About a minute   0.0.0.0:80->80/tcp   docker-rosariosis_web_1
65a193b23df5        sameersbn/postgresql:9.5   "/sbin/entrypoint.sh"    11 minutes ago      Up About a minute   5432/tcp             docker-rosariosis_db_1
```

2. Execute the following command on the database container:
```bash
docker exec -i d8da2ddd29cd pg_dump > rosario_2018_25_10.sql
```

## TODO Some things need some tunings
- Create a registry for the docker containers (English and French)
- Do some tests
- Use Volumes in order to have an easy access to uploaded files (images).
- Improve starting bash script (try to not launching sql script at startup if the database is already filled)

