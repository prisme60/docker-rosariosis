# Dockerfile for RosarioSIS
# https://www.rosariosis.org/
# Best Dockerfile practices: http://crosbymichael.com/dockerfile-best-practices.html

FROM php:5.6-apache

LABEL maintainer="Christian FOUCHER <christian.foucher@gmail.com>"

ENV PGHOST=rosariosisdb \
    PGUSER=postgres \
    PGPASSWORD=postgres \
    PGDATABASE=postgres \
    PGPORT=5432 \
    ROSARIOSIS_YEAR=2018 \
    ROSARIOSIS_LANG='en_US'

# Upgrade packages.
# Install git, Apache2 + PHP + PostgreSQL webserver, sendmail, wkhtmltopdf & others utilities.

# Change date to force an upgrade:
RUN apt-get update && \
    apt-get upgrade -y && \
    mkdir -p /usr/share/man/man1 && \
    mkdir -p /usr/share/man/man7 && \ 
    apt-get install postgresql-client libpq-dev libpng-dev libxml2-dev sendmail locales -y

#------------
RUN curl -O -L https://downloads.wkhtmltopdf.org/0.12/0.12.5/wkhtmltox_0.12.5-1.stretch_amd64.deb && \ 
    apt install ./wkhtmltox_0.12.5-1.stretch_amd64.deb -y && \
    rm wkhtmltox_0.12.5-1.stretch_amd64.deb && \
    ln -fs /usr/local/bin/wkhtmltopdf /usr/bin/wkhtmltopdf && \
    rm -rf /var/lib/apt/lists/* 
#------------

RUN docker-php-ext-configure gd --with-png-dir=/usr --with-jpeg-dir=/usr; \
    docker-php-ext-install -j$(nproc) gd mbstring xml pgsql gettext xmlrpc

# Download and extract rosariosis
ENV ROSARIOSIS_VERSION 'v4.1'
RUN mkdir /usr/src/rosariosis && \
    curl -L https://gitlab.com/francoisjacquet/rosariosis/-/archive/${ROSARIOSIS_VERSION}/rosariosis-${ROSARIOSIS_VERSION}.tar.gz \
    | tar xz --strip-components=1 -C /usr/src/rosariosis && \
    rm -rf /var/www/html && mkdir -p /var/www && \
    ln -s /usr/src/rosariosis/ /var/www/html && chmod 777 /var/www/html &&\
    chown -R www-data:www-data /usr/src/rosariosis

# Copy our custom RosarioSIS configuration file.
COPY conf/config.inc.php /usr/src/rosariosis/config.inc.php
COPY conf/.htaccess /usr/src/rosariosis/.htaccess
COPY bin/init /init


EXPOSE 80

ENTRYPOINT ["/init"]
CMD ["apache2-foreground"]
